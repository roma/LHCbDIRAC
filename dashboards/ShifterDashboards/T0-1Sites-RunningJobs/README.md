# T0-1 Sites Running Jobs
The `T0-1 Sites Running Jobs` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/cfdJGq04z/t0-1-sites-running-jobs?orgId=46) folder of the LHCb grafana organisation.

Contains plots showing the number of running jobs by JobSplitType for tier 0 and all tier 1 sites:
- LCG.CERN.cern
- LCG.CNAF.it
- LCG.GRIDKA.de
- LCG.IN2P3.fr
- LCG.PIC.es
- LCG.RAL.uk
- LCG.RRCKI.ru
- LCG.SARA.nl
- LCG.NIKHEF.nl

A variable is used to repeat a base plot for all the different sites of tier 0 and tier 1.

The datasource for all these plots is WMS indices in elasticsearch.
Note: These plots are not yet validated ([issue](https://github.com/DIRACGrid/WebAppDIRAC/issues/722) blocking validation)
